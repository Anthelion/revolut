package com.revolut.service;

import com.revolut.dao.AccountDao;
import com.revolut.dao.Database;
import com.revolut.dao.TransactionDao;
import com.revolut.dao.impl.DefaultAccountDao;
import com.revolut.dao.impl.DefaultTransactionDao;
import com.revolut.exception.TransactionCreationException;
import com.revolut.exception.TransactionNotFoundException;
import com.revolut.model.Transaction;
import com.revolut.service.impl.DefaultTransactionService;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * @author anthelion
 *         <p> 15/06/2018
 */
public class TransactionServiceTest {
    private static TransactionService transactionService;
    private static AccountDao accountDao;

    @BeforeClass
    public static void init() {
        try {
            Database.getInstance().initializeDB();
            accountDao = new DefaultAccountDao();
            TransactionDao transactionDao = new DefaultTransactionDao();
            transactionService = new DefaultTransactionService(transactionDao, accountDao);
        } catch (Exception e) {

        }
    }

    // make sure all tables are clean for the next test
    @After
    public void cleanup() {
        try {
            Database.getInstance().truncateTable("account");
            Database.getInstance().truncateTable("account_transaction");
        } catch (SQLException e) {

        }
    }

    @Test(expected = TransactionCreationException.class)
    public void test_create_transaction_with_negative_amount() throws SQLException {
        double amount = -10.0;
        transactionService.createTransaction(1, 2, amount);
    }

    @Test(expected = TransactionCreationException.class)
    public void test_send_money_to_oneself() throws SQLException {
        transactionService.createTransaction(1, 1, 10);
    }

    @Test
    public void test_create_transaction() throws SQLException {
        int sender = 1;
        int receiver = 2;
        int amount = 10;

        Transaction transaction = transactionService.createTransaction(sender, receiver, amount);
        assertEquals(sender, transaction.getSender());
        assertEquals(receiver, transaction.getReceiver());
        assertEquals(amount, transaction.getAmount(), 0.0);
    }

    @Test(expected = TransactionNotFoundException.class)
    public void test_commit_nonexistent_transaction() throws SQLException {
        transactionService.commitTransaction(1);
    }

    @Test
    public void test_commit_transaction_insufficient_fund() throws SQLException {
        Transaction transaction = transactionService.createTransaction(1, 2, 5.0);
        String message = transactionService.commitTransaction(transaction.getId());

        assertEquals("Not enough fund", message);
    }

    @Test
    public void test_commit_committed_transaction() throws SQLException {
        int sender = 1, receiver = 2;
        double amount = 10.0;

        Transaction transaction = transactionService.createTransaction(sender, receiver, amount);
        accountDao.updateBalance(sender, amount);
        transactionService.commitTransaction(transaction.getId());
        String message = transactionService.commitTransaction(transaction.getId());
        assertEquals("Double transaction detected", message);
    }

    @Test
    public void test_commit_valid_transaction() throws SQLException {
        int sender = 1;
        int receiver = 2;
        double amount = 5.0;

        Transaction transaction = transactionService.createTransaction(sender, receiver, amount);
        accountDao.updateBalance(sender, amount);
        String message = transactionService.commitTransaction(transaction.getId());
        double balance = accountDao.getBalance(sender);

        assertEquals(String.format("Transaction %d committed", transaction.getId()), message);
        assertEquals(0.0, balance, 0.0);
    }
}