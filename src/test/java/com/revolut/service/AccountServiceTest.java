package com.revolut.service;

import com.revolut.dao.AccountDao;
import com.revolut.dao.Database;
import com.revolut.dao.impl.DefaultAccountDao;
import com.revolut.exception.AccountNotFoundException;
import com.revolut.model.Account;
import com.revolut.service.impl.DefaultAccountService;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * @author anthelion
 *         <p> 15/06/2018
 */
public class AccountServiceTest {
    private static AccountService accountService;
    private static AccountDao accountDao;

    @BeforeClass
    public static void init() {
        try {
            Database.getInstance().initializeDB();
            accountDao = new DefaultAccountDao();
            accountService = new DefaultAccountService(accountDao);
        } catch (Exception e) {

        }
    }

    @After
    public void cleanup() {
        try {
            Database.getInstance().truncateTable("account");
        } catch (SQLException e) {

        }
    }

    @Test
    public void test_get_balance() throws SQLException {
        Account account = accountDao.createAccount(1);
        double balance = accountService.getBalance(account.getUserId());
        assertEquals(0.0, balance, 0.0);
    }

    @Test(expected = AccountNotFoundException.class)
    public void test_get_balance_of_nonexistent_account() throws SQLException {
        accountService.getBalance(1);
    }
}