package com.revolut;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.dao.AccountDao;
import com.revolut.dao.Database;
import com.revolut.dao.TransactionDao;
import com.revolut.dao.impl.DefaultAccountDao;
import com.revolut.dao.impl.DefaultTransactionDao;
import com.revolut.form.TransactionForm;
import com.revolut.model.Account;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionState;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 15/06/2018
 */
@RunWith(VertxUnitRunner.class)
public class MoneyTransferVerticleTest {
    private Vertx vertx;
    private ObjectMapper objectMapper;
    private AccountDao accountDao;
    private TransactionDao transactionDao;
    private int port;

    @Before
    public void setUp(TestContext context) throws SQLException, ClassNotFoundException, IOException {
        Database.getInstance().initializeDB();
        objectMapper = new ObjectMapper();
        accountDao = new DefaultAccountDao();
        transactionDao = new DefaultTransactionDao();

        vertx = Vertx.vertx();
        ServerSocket socket = new ServerSocket(0);
        port = socket.getLocalPort();
        socket.close();

        DeploymentOptions options = new DeploymentOptions()
                .setConfig(new JsonObject().put("http.port", port));

        vertx.deployVerticle(MoneyTransferVerticle.class.getName(), options, context.asyncAssertSuccess());
    }

    @After
    public void teardown(TestContext context) throws SQLException {
        Database.getInstance().truncateTable("account");
        Database.getInstance().truncateTable("account_transaction");
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void test_create_transaction(TestContext context) throws JsonProcessingException {
        Async async = context.async();

        TransactionForm form = new TransactionForm();
        form.setSender(1);
        form.setReceiver(2);
        form.setAmount(5.0);

        String jsonStr = objectMapper.writeValueAsString(form);

        vertx.createHttpClient()
                .post(port, "localhost", "/transaction")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", "" + jsonStr.length())
                .handler(response -> {
                    response.bodyHandler(body -> {
                        String resBody = body.toString();
                        context.assertNotNull(resBody);
                        context.assertEquals(String.format("Transaction %d created", 1), resBody);
                        async.complete();
                    });
                })
                .write(jsonStr)
                .end();
    }

    @Test
    public void test_commit_transaction(TestContext context) throws SQLException {
        Async async = context.async();

        int sender = 1;
        int receiver = 2;
        double amount = 5.0;
        accountDao.createAccount(sender);
        accountDao.createAccount(receiver);
        accountDao.updateBalance(sender, amount);
        Transaction transaction = transactionDao.createTransaction(sender, receiver, amount);

        vertx.createHttpClient()
                .put(port, "localhost", "/transaction/" + transaction.getId())
                .handler(response -> {
                    response.bodyHandler(body -> {
                        String resBody = body.toString();
                        context.assertEquals(String.format("Transaction %d committed", transaction.getId()), resBody);
                        try {
                            TransactionState state = transactionDao.getTransactionState(transaction.getId());
                            context.assertEquals(TransactionState.COMMITTED, state);
                        } catch (SQLException e) {

                        }

                        async.complete();
                    });
                })
                .end();
    }

    @Test
    public void test_get_balance_of_account(TestContext context) throws SQLException {
        Async async = context.async();

        Account account = accountDao.createAccount(1);

        vertx.createHttpClient()
                .get(port, "localhost", "/account/" + account.getUserId())
                .handler(response -> {
                    response.bodyHandler(body -> {
                        String resBody = body.toString();
                        context.assertEquals("0.0", resBody);
                        async.complete();
                    });
                })
                .end();
    }

    @Test
    public void test_get_balance_of_nonexistent_account(TestContext context) throws SQLException {
        Async async = context.async();

        vertx.createHttpClient()
                .get(port, "localhost", "/account/" + 1)
                .handler(response -> {
                    response.bodyHandler(body -> {
                        String resBody = body.toString();
                        context.assertFalse(resBody.matches("-?\\d+"));
                        async.complete();
                    });
                })
                .end();
    }
}