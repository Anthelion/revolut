package com.revolut.model;

/**
 * @author anthelion
 *         <p> 11/06/2018
 */
public class Account {
    private final int userId;

    // balance is not a field of this class as it's dynamic
    public Account(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
