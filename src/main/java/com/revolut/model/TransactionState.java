package com.revolut.model;

/**
 * @author anthelion
 *         <p> 12/06/2018
 */
public enum TransactionState {
    NOT_STARTED, STARTED, COMMITTED;

    public static TransactionState fromOrdinal(int ordinal) {
        return TransactionState.values()[ordinal];
    }
}
