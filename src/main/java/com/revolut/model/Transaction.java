package com.revolut.model;

/**
 * @author anthelion
 *         <p> 12/06/2018
 */
public class Transaction {
    private final int id;
    private final TransactionState state;
    private final int receiver;
    private final int sender;
    private final double amount;

    // transaction state is not part
    public Transaction(int id, TransactionState state, int receiver, int sender, double amount) {
        this.id = id;
        this.state = state;
        this.receiver = receiver;
        this.sender = sender;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public TransactionState getState() {
        return state;
    }

    public int getReceiver() {
        return receiver;
    }

    public int getSender() {
        return sender;
    }

    public double getAmount() {
        return amount;
    }
}
