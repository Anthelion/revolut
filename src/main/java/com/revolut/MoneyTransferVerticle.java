package com.revolut;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.dao.AccountDao;
import com.revolut.dao.Database;
import com.revolut.dao.TransactionDao;
import com.revolut.dao.impl.DefaultAccountDao;
import com.revolut.dao.impl.DefaultTransactionDao;
import com.revolut.form.TransactionForm;
import com.revolut.model.Transaction;
import com.revolut.service.AccountService;
import com.revolut.service.TransactionService;
import com.revolut.service.impl.DefaultAccountService;
import com.revolut.service.impl.DefaultTransactionService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;


/**
 * @author anthelion
 *         <p> 11/06/2018
 */
public class MoneyTransferVerticle extends AbstractVerticle {
    private static final Logger logger = Logger.getLogger(MoneyTransferVerticle.class);
    private TransactionService transactionService;
    private AccountService accountService;
    private ObjectMapper objectMapper;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        // config log4j
        BasicConfigurator.configure();

        // initialize database
        Database.getInstance().initializeDB();

        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(MoneyTransferVerticle.class.getName());
    }

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        TransactionDao transactionDao = new DefaultTransactionDao();
        AccountDao accountDao = new DefaultAccountDao();
        transactionService = new DefaultTransactionService(transactionDao, accountDao);
        accountService = new DefaultAccountService(accountDao);

        objectMapper = new ObjectMapper();
    }

    @Override
    public void start(Future<Void> fut) {
        logger.debug("Verticle initialized");

        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        // create a transaction
        router.post("/transaction").handler(rc -> {
            CompletableFuture<String> future = new CompletableFuture<>();
            String reqBody = rc.getBodyAsString();

            try {
                TransactionForm form = objectMapper.readValue(reqBody, TransactionForm.class);
                Transaction transaction = transactionService.createTransaction(form.getSender(), form.getReceiver(), form.getAmount());
                future.complete(String.format("Transaction %d created", transaction.getId()));
            } catch (IOException e) {
                String message = "Malformatted request body";
                logger.error(message, e.getCause());
                future.complete(message);
            } catch (SQLException e) {
                logger.error(e.getMessage(), e.getCause());
                future.complete(e.getMessage());
            }

            future.thenAccept(message -> rc.response().end(message));
        });

        // commit a transaction
        router.put("/transaction/:id").handler(rc -> {
            CompletableFuture<String> future = new CompletableFuture<>();

            String idStr = rc.request().getParam("id");
            if (!idStr.matches("-?\\d+")) {
                String message = String.format("'%s' is not a valid integer");
                logger.warn(message);
                future.complete(message);
            } else {
                try {
                    String message = transactionService.commitTransaction(Integer.parseInt(idStr));
                    future.complete(message);
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e.getCause());
                    future.complete(e.getMessage());
                }
            }

            future.thenAccept(message -> rc.response().end(message));
        });

        // check the balance of an account
        router.get("/account/:id").handler(rc -> {
            CompletableFuture<String> future = new CompletableFuture<>();

            String idStr = rc.request().getParam("id");
            if (!idStr.matches("-?\\d+")) {
                String message = String.format("'id' must be a valid integer");
                logger.warn(message);
                future.complete(message);
            } else {
                try {
                    Double amount = accountService.getBalance(Integer.parseInt(idStr));
                    future.complete(amount.toString());
                } catch (SQLException e) {
                    future.complete(e.getMessage());
                    logger.error(e.getMessage(), e.getCause());
                }
            }

            future.thenAccept(res -> rc.response().end(res));
        });

        // 8 is a lucky number in China
        server.requestHandler(router::accept)
                .listen(config().getInteger("http.port", 8888), handler -> {
                    if (handler.succeeded()) {
                        fut.complete();
                    } else {
                        fut.fail(handler.cause());
                    }
                });
    }
}
