package com.revolut.exception;

import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 13/06/2018
 */
public class TransactionNotFoundException extends SQLException {
    private static final Logger logger = Logger.getLogger(TransactionNotFoundException.class);

    public TransactionNotFoundException(String errorMessage) {
        super(errorMessage);
        logger.error(errorMessage);
    }
}
