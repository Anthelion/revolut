package com.revolut.form;

/**
 * @author anthelion
 *         <p> 13/06/2018
 * This form is used to initiate a transaction
 */
public class TransactionForm {
    private int sender;
    private int receiver;
    private double amount;

    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
