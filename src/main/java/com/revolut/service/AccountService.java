package com.revolut.service;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 15/06/2018
 */
public interface AccountService {
    double getBalance(int accountId) throws SQLException;
}
