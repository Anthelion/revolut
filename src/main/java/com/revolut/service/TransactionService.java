package com.revolut.service;

import com.revolut.model.Transaction;
import com.revolut.model.TransactionState;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 13/06/2018
 */
public interface TransactionService {
    Transaction createTransaction(int sender, int receiver, double amount) throws SQLException;

    String commitTransaction(int transactionId) throws SQLException;
}
