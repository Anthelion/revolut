package com.revolut.service.impl;

import com.revolut.dao.AccountDao;
import com.revolut.dao.TransactionDao;
import com.revolut.exception.TransactionCreationException;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionState;
import com.revolut.service.TransactionService;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 13/06/2018
 */
public class DefaultTransactionService implements TransactionService {
    private static final Logger logger = Logger.getLogger(DefaultTransactionService.class);
    private final TransactionDao transactionDao;
    private final AccountDao accountDao;

    public DefaultTransactionService(TransactionDao transactionDao, AccountDao accountDao) {
        this.transactionDao = transactionDao;
        this.accountDao = accountDao;
    }

    @Override
    public Transaction createTransaction(int sender, int receiver, double amount) throws SQLException {
        // assume that all accounts exist (get created on the fly)
        accountDao.createAccount(sender);
        accountDao.createAccount(receiver);

        if (amount <= 0) {
            throw new TransactionCreationException("The transaction amount has to be a positive number");
        }
        if (sender == receiver) {
            throw new TransactionCreationException("Sender and receiver must hold different accounts");
        }

        Transaction transaction = transactionDao.createTransaction(sender, receiver, amount);
        return transaction;
    }

    @Override
    public String commitTransaction(int transactionId) throws SQLException {
        Transaction transaction = transactionDao.getTransaction(transactionId);
        String message;

        // prevent double transactions
        if (transaction.getState() == TransactionState.COMMITTED) {
            message = "Double transaction detected";
            logger.warn(message);
            return message;
        }

        // double check that both accounts actually exist
        if (accountDao.accountsExist(transaction.getSender(), transaction.getReceiver())) {
            // make sure the sender has enough fund in the account
            if (accountDao.getBalance(transaction.getSender()) >= transaction.getAmount()) {
                transactionDao.startTransaction();
                accountDao.updateBalance(transaction.getSender(), -transaction.getAmount());
                accountDao.updateBalance(transaction.getReceiver(), transaction.getAmount());
                transactionDao.commitTransaction(transactionId);
                message = String.format("Transaction %d committed", transactionId);
                logger.debug(message);
            } else {
                message = "Not enough fund";
                logger.warn(message);
            }
        } else {
            message = "Either sender or receiver account does not exist";
            logger.warn(message);
        }

        return message;
    }

}
