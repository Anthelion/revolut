package com.revolut.service.impl;

import com.revolut.dao.AccountDao;
import com.revolut.service.AccountService;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 15/06/2018
 */
public class DefaultAccountService implements AccountService {
    private final AccountDao accountDao;

    public DefaultAccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public double getBalance(int accountId) throws SQLException {
        return accountDao.getBalance(accountId);
    }
}
