package com.revolut.dao;

import org.apache.log4j.Logger;
import org.sqlite.SQLiteConfig;

import java.sql.*;

/**
 * @author anthelion
 *         <p> 12/06/2018
 *         All tables are initialized in this class
 */
public class Database {
    private static final Logger logger = Logger.getLogger(Database.class);
    private static final Database instance;
    private Connection conn;

    private static final String createAccountTableQuery = "CREATE TABLE IF NOT EXISTS account (user_id INTEGER PRIMARY KEY NOT NULL," +
            "balance REAL NOT NULL DEFAULT 0," +
            "CHECK (balance >= 0))";

    private static final String createTransactionTableQuery = "CREATE TABLE IF NOT EXISTS account_transaction (transaction_id INTEGER PRIMARY KEY NOT NULL," +
            "state INTEGER NOT NULL DEFAULT 0," +
            "sender INTEGER NOT NULL," +
            "receiver INTEGER NOT NULL," +
            "amount REAL NOT NULL CHECK (amount > 0)," +
            "FOREIGN KEY (sender) REFERENCES account (user_id) ON DELETE CASCADE," +
            "FOREIGN KEY (receiver) REFERENCES account (user_id) ON DELETE CASCADE)";

    private static final String truncateTableQuery = "DELETE FROM %s";

    // make sure that there's only a single instance of Database
    // hence a single instance of db connection
    static {
        instance = new Database();
    }

    private Database() {
    }

    public static Database getInstance() {
        return instance;
    }

    public void initializeDB() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");

        if (conn == null) {
            // enforce foreign key
            SQLiteConfig sqLiteConfig = new SQLiteConfig();
            sqLiteConfig.enforceForeignKeys(true);

            // enable in-memory mode
            conn = DriverManager.getConnection("jdbc:sqlite::memory:", sqLiteConfig.toProperties());

            // use prepareStatement as execution plan gets cached, hence better performance
            // table creation
            createAccountTable();
            createTransactionTable();
        }
    }

    public PreparedStatement prepareStatement(String query) throws SQLException {
        return conn.prepareStatement(query);
    }

    public boolean execUpdate(PreparedStatement statement) throws SQLException {
        try {
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("An error has occurred while executing statement", e.getCause());
            return false;
        } finally {
            statement.close();
        }

        return true;
    }

    public ResultSet execQueryWithResult(PreparedStatement statement) throws SQLException {
        ResultSet resultSet = null;

        try {
            resultSet = statement.executeQuery();
        } catch (SQLException e) {
            logger.error("An error has occurred while executing select statement", e.getCause());
        }

        return resultSet;
    }

    public void disableAutoCommit() throws SQLException {
        conn.setAutoCommit(false);
    }

    public void commit() throws SQLException {
        conn.commit();
    }

    public void closeConnection() throws SQLException {
        if (!conn.isClosed()) {
            conn.close();
        }
    }

    public boolean truncateTable(String tableName) throws SQLException {
        String query = String.format(truncateTableQuery, tableName);
        PreparedStatement statement = prepareStatement(query);
        return execUpdate(statement);
    }

    // private

    private void createAccountTable() throws SQLException {
        PreparedStatement statement = prepareStatement(createAccountTableQuery);
        logger.debug("Creating account table...");
        boolean updated = execUpdate(statement);
        logger.debug(updated ? "Table 'account' created" : "Failed to create table 'account'");
    }

    private void createTransactionTable() throws SQLException {
        PreparedStatement statement = prepareStatement(createTransactionTableQuery);
        logger.debug("Creating account_transaction table...");
        boolean updated = execUpdate(statement);
        logger.debug(updated ? "Table 'account_transaction' created" : "Failed to create table 'account_transaction'");
    }
}
