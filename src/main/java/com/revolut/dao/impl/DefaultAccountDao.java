package com.revolut.dao.impl;

import com.revolut.dao.AccountDao;
import com.revolut.dao.Database;
import com.revolut.exception.AccountCreationException;
import com.revolut.exception.AccountNotFoundException;
import com.revolut.exception.UpdateBalanceException;
import com.revolut.model.Account;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 13/06/2018
 */
public class DefaultAccountDao implements AccountDao {
    // insert if only the account does not exist
    private static final String createAccountQuery = "INSERT INTO account (user_id) SELECT ? WHERE NOT EXISTS (SELECT user_id FROM account WHERE user_id = ?)";
    private static final String getBalanceQuery = "SELECT balance FROM account WHERE user_id = ?";
    private static final String updateBalanceQuery = "UPDATE account SET balance = balance + ? WHERE user_id = ?";
    private static final String checkAccountsQuery = "SELECT count(*) FROM account WHERE user_id IN (?, ?)";

    @Override
    public Account createAccount(int accountId) throws SQLException {
        PreparedStatement statement = Database.getInstance().prepareStatement(createAccountQuery);
        statement.setInt(1, accountId);
        statement.setInt(2, accountId);
        boolean updated = Database.getInstance().execUpdate(statement);
        if (!updated) {
            throw new AccountCreationException(String.format("Failed to create account %d", accountId));
        }

        return new Account(accountId);
    }

    @Override
    public boolean accountsExist(int accountId1, int accountId2) throws SQLException {
        PreparedStatement statement = Database.getInstance().prepareStatement(checkAccountsQuery);
        statement.setInt(1, accountId1);
        statement.setInt(2, accountId2);
        ResultSet resultSet = Database.getInstance().execQueryWithResult(statement);
        if (resultSet.next()) {
            if (resultSet.getInt(1) != 2) return false;
        }

        return true;
    }

    @Override
    public double getBalance(int accountId) throws SQLException {
        PreparedStatement statement = Database.getInstance().prepareStatement(getBalanceQuery);
        statement.setInt(1, accountId);
        ResultSet resultSet = Database.getInstance().execQueryWithResult(statement);
        double balance;

        if (resultSet.next()) {
            balance = resultSet.getDouble(1);
        } else {
            throw new AccountNotFoundException(String.format("Account %d not found", accountId));
        }
        statement.close();

        return balance;
    }

    @Override
    public void updateBalance(int accountId, double amount) throws SQLException {
        PreparedStatement statement = Database.getInstance().prepareStatement(updateBalanceQuery);
        statement.setDouble(1, amount);
        statement.setInt(2, accountId);
        boolean updated = Database.getInstance().execUpdate(statement);

        if (!updated) {
            throw new UpdateBalanceException(String.format("Failed to update the balance of account %d", accountId));
        }
    }
}
