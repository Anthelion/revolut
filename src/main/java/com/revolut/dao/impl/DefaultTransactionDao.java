package com.revolut.dao.impl;

import com.revolut.dao.Database;
import com.revolut.dao.TransactionDao;
import com.revolut.exception.TransactionCreationException;
import com.revolut.exception.TransactionNotFoundException;
import com.revolut.model.Transaction;
import com.revolut.model.TransactionState;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 12/06/2018
 */
public class DefaultTransactionDao implements TransactionDao {
    private static final Logger logger = Logger.getLogger(DefaultTransactionDao.class);

    private static final String getTransactionStateQuery = "SELECT state FROM account_transaction WHERE transaction_id = ?";
    private static final String getTransactionQuery = "SELECT transaction_id, state, receiver, sender, amount FROM account_transaction WHERE transaction_id = ?";
    private static final String insertTransactionQuery = "INSERT INTO account_transaction (sender, receiver, amount, state) VALUES (?, ?, ?, ?)";
    private static final String updateTransactionStateQuery = "UPDATE account_transaction SET state = ? WHERE transaction_id = ?";

    @Override
    public TransactionState getTransactionState(int transactionId) throws SQLException {
        logger.debug(String.format("Retrieving the state of transaction %d", transactionId));

        TransactionState state;
        PreparedStatement statement = Database.getInstance().prepareStatement(getTransactionStateQuery);
        statement.setInt(1, transactionId);
        ResultSet resultSet = Database.getInstance().execQueryWithResult(statement);

        if (resultSet.next()) {
            state = TransactionState.fromOrdinal(resultSet.getInt(1));
        } else {
            throw new TransactionNotFoundException(String.format("Transaction %d not found", transactionId));
        }

        resultSet.close();

        return state;
    }

    @Override
    public Transaction getTransaction(int transactionId) throws SQLException {
        logger.debug(String.format("Retrieving transaction %d", transactionId));

        PreparedStatement statement = Database.getInstance().prepareStatement(getTransactionQuery);
        statement.setInt(1, transactionId);
        ResultSet resultSet = Database.getInstance().execQueryWithResult(statement);
        Transaction transaction;

        if (resultSet.next()) {
            transaction = new Transaction(
                    resultSet.getInt(1),
                    TransactionState.fromOrdinal(resultSet.getInt(2)),
                    resultSet.getInt(3),
                    resultSet.getInt(4),
                    resultSet.getDouble(5)
            );
        } else {
            throw new TransactionNotFoundException(String.format("Transaction %d not found", transactionId));
        }

        return transaction;
    }

    /**
     * 1. amount has to be positive
     * 2. both sender and receiver accounts must exist
     */
    @Override
    public Transaction createTransaction(int sender, int receiver, double amount) throws SQLException {
        logger.debug(String.format("Initiating transaction of amount %f from account %d to account %d", amount, sender, receiver));

        PreparedStatement statement = Database.getInstance().prepareStatement(insertTransactionQuery);
        statement.setInt(1, sender);
        statement.setInt(2, receiver);
        statement.setDouble(3, amount);
        statement.setInt(4, TransactionState.STARTED.ordinal());
        boolean updated = Database.getInstance().execUpdate(statement);
        ResultSet resultSet = statement.getGeneratedKeys();
        Transaction transaction;

        if (updated && resultSet.next()) {
            transaction = new Transaction(resultSet.getInt(1), TransactionState.STARTED, receiver, sender, amount);
            resultSet.close();
        } else {
            throw new TransactionCreationException(String.format("Failed to initiate transaction from %d to %d of amount %f", sender, receiver, amount));
        }
        return transaction;
    }

    @Override
    public void startTransaction() throws SQLException {
        Database.getInstance().disableAutoCommit();
    }

    @Override
    public void commitTransaction(int transactionId) throws SQLException {
        Database.getInstance().commit();
        PreparedStatement statement = Database.getInstance().prepareStatement(updateTransactionStateQuery);
        statement.setInt(1, TransactionState.COMMITTED.ordinal());
        statement.setInt(2, transactionId);
        Database.getInstance().execUpdate(statement);
    }
}
