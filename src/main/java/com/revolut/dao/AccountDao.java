package com.revolut.dao;

import com.revolut.model.Account;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 13/06/2018
 */
public interface AccountDao {
    Account createAccount(int accountId) throws SQLException;

    boolean accountsExist(int accountId1, int accountId2) throws SQLException;

    double getBalance(int accountId) throws SQLException;

    void updateBalance(int accountId, double amount) throws SQLException;
}
