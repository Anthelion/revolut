package com.revolut.dao;

import com.revolut.model.Transaction;
import com.revolut.model.TransactionState;

import java.sql.SQLException;

/**
 * @author anthelion
 *         <p> 13/06/2018
 */
public interface TransactionDao {
    TransactionState getTransactionState(int transactionId) throws SQLException;

    Transaction getTransaction(int transactionId) throws SQLException;

    Transaction createTransaction(int sender, int receiver, double amount) throws SQLException;

    void startTransaction() throws SQLException;

    void commitTransaction(int transactionId) throws SQLException;
}
